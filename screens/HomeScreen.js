import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <ScrollView>
            <Text color="white">Home Screen</Text>
              <Button
                title="My Information"
                onPress={() => this.props.navigation.navigate('Information')}
              />
              <Button
                title="Activity Indicator" color="black"
                onPress={() => this.props.navigation.navigate('ActivityIndicator')}
              />
              <Button
                title="DrawerLayout"
                onPress={() => this.props.navigation.navigate('DrawerLayout')}
              />
              <Button
                title="Image Screen" color="black"
                onPress={() => this.props.navigation.navigate('ImageScreen')}
              />
              <Button
                title="Keyboard Avoid"
                onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
              />
              <Button
                title="List View" color="black"
                onPress={() => this.props.navigation.navigate('ListView')}
              />
              <Button
                title="Modal Screen" 
                onPress={() => this.props.navigation.navigate('ModalScreen')}
              />
              <Button
                title="Picker Screen" color="black"
                onPress={() => this.props.navigation.navigate('PickerScreen')}
              />
              <Button
                title="Progress Bar" 
                onPress={() => this.props.navigation.navigate('ProgressBar')}
              />
              <Button
                title="Refresh Control" color="black"
                onPress={() => this.props.navigation.navigate('RefreshControl')}
              />
              <Button
                title="Scroll View" 
                onPress={() => this.props.navigation.navigate('ScrollView')}
              />
              <Button
                title="Section List" color="black"
                onPress={() => this.props.navigation.navigate('SectionList')}
              />
              <Button
                title="Slider"
                onPress={() => this.props.navigation.navigate('Slider')}
              />
              <Button
                title="Status Bar" color="black"
                onPress={() => this.props.navigation.navigate('StatusBar')}
              />
              <Button
                title="Switch"
                onPress={() => this.props.navigation.navigate('Switch')}
              />
              <Button
                title="Text Input" color="black"
                onPress={() => this.props.navigation.navigate('TextInput')}
              />
              <Button
                title="Text"
                onPress={() => this.props.navigation.navigate('Text')}
              />
              <Button
                title="Touchable Highlight" color="black"
                onPress={() => this.props.navigation.navigate('TouchableHighlight')}
              />
              <Button
                title="Touchable Native Feedback"
                onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
              />
              <Button
                title="Touchable Opacity" color="black"
                onPress={() => this.props.navigation.navigate('TouchableOpacity')}
              />
              <Button
                title="View Pager"
                onPress={() => this.props.navigation.navigate('ViewPager')}
              />
              <Button
                title="View" color="black"
                onPress={() => this.props.navigation.navigate('View')}
              />
              <Button
                title="WebView"
                onPress={() => this.props.navigation.navigate('WebView')}
              />
            </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default HomeScreen;
