import React, { Component } from 'react';
import { WebView } from 'react-native';

export default class WebViewScreen extends Component {
  render() {
    return (
      <WebView
        originWhitelist={['*']}
        source={{ html: '<h1>Michael Regis</h1><h1>Header 1</h1> <br> <h2>Header 2</h2> <br> <h1>Header 3</h1> <br> <h2>Header 4</h2> <br> <h1>Header 5</h1> <br> <h2>Header 6</h2>' }}
      />
    );
  }
}
