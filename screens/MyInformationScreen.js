import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class MyInformationScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text style={fontSize.container}>My Information></Text>
            <Text>Name: REGIS, Michael Juanito Jr. T.</Text>
            <Text>Section: BSIT-3B</Text>
            <Text>Course: BSIT</Text>
            <Text>Subject: IT Professional Elective I</Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

const fontSize = StyleSheet.create({
    container: {
      fontSize: 20, 
    },
  });


export default MyInformationScreen;
